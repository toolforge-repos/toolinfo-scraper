Toolinfo scraper
================

The Wikimedia movement has invested a lot of effort into building lists of
tools that can be used to make working with Wikimedia wikis a nicer process.

The toolinfo.json standard is an evolving attempt to create a standardized way
to document these tools. Toolhub is a platform that is collecting toolinfo
records into a database to allow for curation and remixing. Toolinfo scraper
hopes to help with the transition from lists in wiki pages to data in Toolhub
by scraping content from some well known lists and formatting them into
toolinfo.json data files that can be consumed by Toolhub and other toolinfo
aware products.

Parsers
-------

* enwiki_userscripts.py - Parser for [[w:en:Wikipedia:User_scripts/List]]

License
-------
[GNU GPLv3+](https://www.gnu.org/copyleft/gpl.html "GNU GPLv3+")
