# This file is part of the Toolinfo scraper application.
#
# Copyright (C) 2021 Bryan Davis and contributors
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
"""Create toolinfo.json data from [[w:en:Wikipedia:User_scripts/List]]."""
import argparse
import json
import logging
import re
import sys
import unicodedata
import urllib.parse

import mwparserfromhell

import pywikibot

import requests


# Hacks for T272088: turn log level way down
pywikibot.output(__file__)
logging.getLogger("pywiki").setLevel(logging.WARNING)

logger = logging.getLogger(__name__)


TOOL_TYPE = "user script"
LICENSE = "CC-BY-SA-4.0"  # enwiki default license


RE_SLUG_REMOVE = re.compile(r"[^\w\s-]")
RE_SLUG_REPLACE = re.compile(r"[-\s]+")
RE_EXT = re.compile(r"\.(css|js)$", re.IGNORECASE)
RE_USER = re.compile(r"User:(?P<username>[^/]+)/")


IGNORE_NAMES = {
    "dewiki-tmg-autoformatter": True,
}


def slugify(value):
    """Convert a string to a slug."""
    value = unicodedata.normalize("NFKC", value)
    value = RE_SLUG_REMOVE.sub("", value).strip().lower()
    return RE_SLUG_REPLACE.sub("-", value)


def template_to_toolinfo(template, site):
    """Create a toolinfo record from a [[w:en:Template:User script table row]].

    Some logic borrowed from [[w:en:Module:User script table row]].
    """

    def _get_value(name, default=None):
        """Get a value from the template."""
        param = template.get(name, default=default)
        if param != default:
            val = param.value.strip()
            # Return default if value is empty str too
            return val if val else default
        return param

    def _full_url(page):
        """Compute a full url for a page."""
        # There is nothing technically wrong with the URLs that Page creates,
        # but they are visually overescaped. This method does not force
        # quoting of all special chars in the title, instead only quoting
        # unsafe chars.
        return page.site.base_url(
            page.site.article_path
            + urllib.parse.quote_plus(page.title(), safe="/:").replace(
                "+", "_"
            )
        )

    code = _get_value("code")
    doc = _get_value("doc")
    name = _get_value("name")
    desc = _get_value("desc")
    instruction = _get_value("instruction")
    skin = _get_value("skin", default="common")
    backlink = _get_value("backlink")

    if code is None and doc is None:
        # No way to figure out where the script actually is, so skip
        return None

    if code is None:
        code = doc + ".js"

    if doc is None:
        title = RE_EXT.sub("", code)
        page = pywikibot.Page(site, title)
        if page.exists():
            doc = title

    if name is None:
        if doc:
            name = doc.split("/")[-1]
        else:
            name = RE_EXT.sub("", code).split("/")[-1]

    if desc is None:
        # Makes kind of redundant looking output, but also passes data
        # validation so better than not doing anything.
        desc = name

    try:
        # Try to extract owning user from page title
        m = RE_USER.search(code)
    except TypeError:
        logger.exception("Failed to search '%s' with %s", code, RE_USER)
        m = None

    if m is not None:
        author = m.group("username")
    else:
        # First editor of the page wins!
        try:
            page = pywikibot.Page(site, code)
            author = next(page.revisions(reverse=True, total=1))["user"]
        except KeyError:
            logger.exception("Failed to get history for page '%s'", code)
            author = None

    page = pywikibot.Page(site, code)
    toolinfo = {
        "name": slugify(
            "{site}-{user}-{tool}".format(
                site=page.site.dbName(),
                user=author,
                tool=name,
            )
        ),
        "title": name,
        "description": desc,
        "url": _full_url(page),
        "author": author,
        "tool_type": TOOL_TYPE,
        "license": LICENSE,
    }
    if doc:
        toolinfo["user_docs_url"] = [
            {
                "url": _full_url(pywikibot.Page(site, doc)),
                "language": "en",
            }
        ]

    if IGNORE_NAMES.get(toolinfo["name"], False):
        return None

    return toolinfo


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-v",
        "--verbose",
        action="count",
        default=0,
        dest="loglevel",
        help="Increase logging verbosity",
    )
    parser.add_argument(
        "-o",
        "--output",
        type=argparse.FileType("w"),
        default=sys.stdout,
        help="Directs the output to a name of your choice",
    )
    args = parser.parse_args()

    logging.basicConfig(
        level=max(logging.DEBUG, logging.WARNING - (10 * args.loglevel)),
        format="%(asctime)s %(name)-12s %(levelname)-8s: %(message)s",
        datefmt="%Y-%m-%dT%H:%M:%SZ",
    )
    logging.captureWarnings(True)

    # Hacks for T272088: Set log level to match root logger
    logging.getLogger("pywiki").setLevel(
        logging.getLogger().getEffectiveLevel()
    )

    site = pywikibot.Site("en", "wikipedia")
    page = pywikibot.Page(site, "Wikipedia:User_scripts/List")
    text = page.text
    code = mwparserfromhell.parse(text)
    toolinfo = []
    for template in code.filter_templates():
        if template.name.matches("User script table row"):
            try:
                info = template_to_toolinfo(template, site)
                if info:
                    toolinfo.append(info)
            except Exception:
                logger.exception("Failed to parse %s", template)

    args.output.write(json.dumps(toolinfo, indent=2))


main()
